export interface IConfig {
    logLevel: string
    logRequestsEnabled: boolean
    cleanHeaders: boolean
    port: number
    corsOrigins: string[]
}

export function getConfigFromEnv() {
    const config: IConfig = {
        logLevel: process.env.LOG_LEVEL,
        logRequestsEnabled: process.env.LOG_REQUEST_ENABLED === 'true',
        cleanHeaders: process.env.CLEAN_HEADERS === 'true',
        corsOrigins: process.env.CORS_ORIGINS.split(',').map(o => o.trim()),
        port: parseInt(process.env.PORT, 10)
    };
    return config
}
