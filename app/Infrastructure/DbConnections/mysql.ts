const Sequelize = require('sequelize');
import db from "../Config/db";

const {mysql} = db;

// connection using env variables
const MysqlConn = new Sequelize(mysql.database, mysql.username, mysql.password, {
    host: mysql.host,
    dialect: mysql.dialect
});

MysqlConn
    .authenticate()
    .then(() => {
        console.log('Mysql Connection has been established successfully.');
    })
    .catch((err: any) => {
        console.error('Unable to connect to the database:', err);
    });


MysqlConn.sync().then((result: any) => {
    console.log("Database has synced successfully!");
}).catch((err: any) => {
    console.log("Database sync Error!");

});


export default MysqlConn;
