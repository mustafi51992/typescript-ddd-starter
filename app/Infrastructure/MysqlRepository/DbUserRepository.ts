import MysqlConn from "../DbConnections/mysql";
import UserRepository from "../../Domain/Core/UserRepository";
import User from "../../Domain/Core/User";
import DbOperationFailureException from "./Exceptions/DataException";

const Sequelize = require("sequelize");

const UserModel = MysqlConn.define('User', {
    userId: {
        type: Sequelize.UUID,
        primaryKey: true
    },
    firstName: {type: Sequelize.STRING},
    lastName: {type: Sequelize.STRING},
    email: {type: Sequelize.STRING},
    password: {type: Sequelize.STRING},
    createdAt: {type: Sequelize.DATE, defaultValue: Sequelize.NOW},
    updatedAt: {type: Sequelize.DATE, defaultValue: Sequelize.NOW}
});

class DbUserRepository implements UserRepository {
    /**
     * Add User in database
     * @param {User} user
     * @return Promise<boolean>
     */
    async add(user: User): Promise<boolean> {
        try {
            await UserModel.find(user.toStoreObject());
            return true
        } catch {
            throw new DbOperationFailureException();
        }
    }

    /**
     * Find All User from database
     * @return {User[]}
     */
    async findAll(): Promise<User[]> {
        try {
            const userObjs = await UserModel.find({});
            return userObjs.map((userObj: object) => {
                return User.createFromObject(userObj);
            });
        } catch {
            throw new DbOperationFailureException();
        }
    }

    /**
     * Find User by Email Id and Password from database
     * @param {string} email
     * @param {string} password
     * @return {User}
     */
    async findByEmailAndPass(email: string, password: string): Promise<User> {
        try {
            const userObj = await UserModel.find({
                where: {
                    email,
                    password
                }
            });
            return User.createFromObject(userObj);
        } catch {
            throw new DbOperationFailureException();
        }
    }

    /**
     * Find User by User Id from database
     * @param {string} userId
     * @return {User}
     */
    async findByUserId(userId: string): Promise<User> {
        try {
            const userObj = await UserModel.find({
                where: {
                    userId
                }
            });
            return User.createFromObject(userObj);
        } catch {
            throw new DbOperationFailureException();
        }
    }

    /**
     * Delete User from database by User Id
     * @param {string} userId
     * @return Promise<boolean>
     */
    async remove(userId: string): Promise<boolean> {
        try {
            await UserModel.destroy({
                where: {
                    userId
                }
            });
            return true
        } catch {
            throw new DbOperationFailureException();
        }
    }

    /**
     * Update User in database
     * @param {User} user
     * @return Promise<boolean>
     */
    async update(user: User): Promise<boolean> {
        try {
            await UserModel.update(user.toStoreObject(), {
                where: {
                    userId: user.userId
                }
            });
            return true
        } catch {
            throw new DbOperationFailureException();
        }
    }

}

export default new DbUserRepository();
